import Vue from 'vue'
import Router from 'vue-router'
import List from './components/List.vue'
import Detail from './components/Detail.vue'
import Profile from './components/Profile.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'list',
      component: List
    },
    {
      path: '/detail/:id',
      name: 'detail',
      component: Detail,
      props: true
    }, 
    {
      path: '/profile',
      name: 'profile',
      component: Profile
    }
  ]
})
