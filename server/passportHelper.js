const LocalStrategy = require('passport-local').Strategy
const User = require('./models/User').User
const bcrypt = require('bcrypt')


exports.configurePassport = (passport) => {

	const localStrategy = new LocalStrategy({ 
		usernameField: 'email' 
	},
	async (email, password, done) => {
	    //console.log('Inside local strategy callback')
	    // here is where you make a call to the database
	    // to find the user based on their username or email address
	    // for now, we'll just pretend we found that it was users[0]

	    if (!email) {
		    return done(null, false, { message: 'Email required.' })
	    }

	    if (!password) {
		    return done(null, false, { message: 'Password required.' })
	    }
	    try {
	    	let user = await User.findOne({
		    	email: {$regex: email, $options: 'i'}
		    })
		    

		    if (!user) {
		    	console.log('Incorrect username.')
		        return done(null, false, { message: 'Incorrect username.' })
		    }

		    let valid = await isValidPassword(user, email, password)

	    	if(valid) {
		      return done(null, user)
		    } else {
		    	console.log('Incorrect password.')
        		return done(null, false, { message: 'Incorrect password.' })
		    }
		    
		    
	    }
	    catch(e) {
	    	console.log(e)
	    	return done(e)
	    }
	    
	})

	passport.use(localStrategy)

	// tell passport how to serialize the user
	passport.serializeUser((user, done) => {
	  done(null, user._id)
	})

	passport.deserializeUser(async (id, done) => {

	  try {
	  	 let user = await User.findById(id)
	     done(false, user);
	  }
	  catch(e) {
	  	console.log(e)
	  	done(e)
	  }
	  
	})
}

isValidPassword = async (user, email, password) => {
	let validPassword = await bcrypt.compare(password, user.password) 
	return email.toLowerCase() === user.email.toLowerCase() && validPassword
}
