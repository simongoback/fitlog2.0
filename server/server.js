const express = require('express')
const session = require('express-session')
const passport = require('passport')
const exphbs  = require('express-handlebars')
const flash = require('express-flash')
const path = require('path')

require('dotenv').config()
require('./mongoose')

const app = express()
const port = process.env.PORT || 3000
const clientDir = path.join(__dirname, '../vue-default/dist')
const publicDir = path.join(__dirname, './public')
const passportHelper = require('./passportHelper')
const userRouter = require('./routes/user')
const commonRouter = require('./routes/common')

const requestLogger = (req, res, next) => {
	console.log(req.method + ' ' + req.path)
	next()
}

passportHelper.configurePassport(passport)

app.engine('handlebars', exphbs())
app.set('view engine', 'handlebars')

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(requestLogger)
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
}))
app.use(flash())
app.use(passport.initialize())
app.use(passport.session())

const isLoggedIn = (req, res, next) => {
	if(req.isAuthenticated()) {
		next()
	} else {
		console.log(`User authenticated: ${req.isAuthenticated()}`)

		if (req.url !== '/login') {
			req.session.returnTo = req.url
            res.redirect('/login')
        } else {
            res.redirect('/login')
        }
	}
}

app.get('/getusers', userRouter.getUsers)

//logout
app.get('/logout', function(req, res){
  req.logout()
  res.redirect('/login')
})

//login
app.get('/login', (req, res) => {
  res.render('login')
})
// app.post('/login', 
// 	passport.authenticate('local', { successRedirect: '/',
//                                    failureRedirect: '/login',
//                                    failureFlash: true })
// )

app.post('/login',
	passport.authenticate('local', {
		failureRedirect: '/login',
		failureFlash: true
	}),
	(req, res) => {
		let returnTo;

		if (req.session.returnTo) {
			returnTo = req.session.returnTo
			req.session.returnTo = null
		} 
		
		res.redirect(returnTo || '/')
		
	})


//signup
app.get('/signup', (req, res) => {
	res.render('signup')
})
app.post('/signup', userRouter.signup)


//home
app.get('/', isLoggedIn, (req, res) => {
	res.sendFile(path.join(clientDir, 'index.html'))
})

app.use(express.static(clientDir))
app.use(express.static(publicDir))

//user workouts
app.get('/workouts', isLoggedIn, userRouter.getWorkouts)
app.get('/workout/:id', isLoggedIn, userRouter.getWorkout)
app.post('/workout/:id', isLoggedIn, userRouter.upsertWorkout)
app.put('/workout', isLoggedIn, userRouter.createWorkout)
app.delete('/workout/:id', isLoggedIn, userRouter.deleteWorkout)
app.get('/preferences', isLoggedIn, userRouter.getUserPreferences)
app.post('/preferences/', isLoggedIn, userRouter.saveUserPreferences)


app.get('/globalexercises', isLoggedIn, commonRouter.getGlobalWorkouts)
//error handler
app.use(function (err, req, res, next) {
	console.error(err.stack)
	res.status(500).send('Something broke!\n')
})

app.listen(port, () => {
	console.log(`Server running at ${port} ...`)
})

