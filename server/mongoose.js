const mongoose = require('mongoose')

mongoose.connect(process.env.DB_CLUSTER, {
	user: process.env.DB_USER, 
	pass: process.env.DB_PWD,
	useNewUrlParser: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))

db.once('open', function() {
  console.log('MongoDB connection successful')
})

exports.mongoose = mongoose