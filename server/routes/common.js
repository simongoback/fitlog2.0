const GlobalExercises = require('../models/GlobalExercises').GlobalExercises
const UserPreferences = require('../models/UserPreferences').UserPreferences


exports.getGlobalWorkouts = async (req, res) => {
	try {
		let globalEx =  GlobalExercises.find({})
		let pref =  UserPreferences.findOne({
			user_id: req.user._id
		}, 'exercises')

		let result = await Promise.all([pref, globalEx])
		let data = []


		if (result[0]) {
			data = data.concat(result[0].exercises)
		}

		if (result[1]) {
			data = data.concat(result[1])
		}

		if (data.length) {
			data.sort((a, b) => a.name.localeCompare(b.name))
		}
	    res.send({
	    	success: true, 
	    	data: data
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}