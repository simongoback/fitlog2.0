const bcrypt = require('bcrypt')
const mongoose = require('../mongoose').mongoose
const User = require('../models/User').User
const Workout = require('../models/Workout').Workout
const DeletedWorkout = require('../models/DeletedWorkout').DeletedWorkout
const UserPreferences = require('../models/UserPreferences').UserPreferences



exports.signup = async(req, res) => {

	let {firstName,
		lastName,
		email,
		password,
		confirmPassword} = req.body

	if (password !== confirmPassword) {
		req.flash('error', 'Passwords do not match')
		res.redirect('/signup')
	} else {
		try {
			password = await bcrypt.hash(password, 10)

			let user = await User.create({
				firstName: firstName.toLowerCase(),
				lastName: lastName.toLowerCase(),
				email: email.toLowerCase(),
				password
			}) 

			UserPreferences.create({
				distanceUnit: 'km',
				timeUnit: 'min',
				weightUnit: 'kg',
				user_id: user._id
			})

			req.flash('success', 'User created.')
			res.redirect('/login')
		}
		catch(err) {
			console.log(err)
			req.flash('error', 'Could not create user. Try again.')
			res.redirect('/signup')
		}
	}
}

exports.getWorkouts = async (req, res) => {
	let id = req.user._id
	console.log(req.params)

	try {
		let result = await Workout.find({
	    	user_id: id
	    }).sort({date: -1, creation_date: -1})	

	    res.send({
	    	success: true, 
	    	data: result
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}

exports.getUserPreferences = async (req, res) => {
	let id = req.user._id

	try {
		let result = await UserPreferences.findOne({
	    	user_id: id
	    })	

	    res.send({
	    	success: true, 
	    	data: result
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}

exports.saveUserPreferences = async (req, res) => {
	req.body.preferences.user_id = req.user._id;

	try {
		let result = await UserPreferences.updateOne({
			user_id: req.user._id
		},req.body.preferences,{
			upsert:true,
			omitUndefined: true,
			overWrite: true
		})	

	    res.send({
	    	success: true, 
	    	data: result
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}

exports.getWorkout = async (req, res) => {

	try {
		let result = await Workout.findById(req.params.id)	

	    res.send({
	    	success: true, 
	    	data: result
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}

exports.getUsers = async (req, res) => {
	try {
		let result = await User.find()	

	    res.send({
	    	success: true, 
	    	data: result
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}

exports.upsertWorkout = async (req, res) => {
	try {
		let result = await Workout.updateOne({
			_id: req.params.id
		}, req.body.workout, {
			upsert:true,
			omitUndefined: true,
			overWrite: true
		})

		res.send({
	    	success: true, 
	    	data: result
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}

exports.createWorkout = async (req, res) => {
	try {
		req.body.workout.user_id = req.user._id;
		req.body.workout.creation_date = new Date();
		let result = await Workout.create(req.body.workout)

		res.send({
	    	success: true, 
	    	data: result
	    })
	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}

exports.deleteWorkout = async (req, res) => {
	try {
		console.log('Delete workout')
		let workout = await Workout.findById(req.params.id)
		delete workout._doc._id

		console.log(workout)
		await Promise.all([
			DeletedWorkout.create(workout._doc),
			Workout.deleteOne({_id: req.params.id})
		])

		res.send({
	    	success: true
	    })

	}
	catch(err) {
		console.log(err)
		res.send({
			success: false,
			data: err
		})
	}
}