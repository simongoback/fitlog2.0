const mongoose = require('../mongoose').mongoose

const deletedWorkoutSchema = new mongoose.Schema({
  type: String,
  exercise: Array,
  date: Date,
  user_id: mongoose.ObjectId
})

exports.DeletedWorkout = mongoose.model('deletedWorkouts', deletedWorkoutSchema)