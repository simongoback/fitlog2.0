const mongoose = require('../mongoose').mongoose

const globalExercisesSchema = new mongoose.Schema({
  type: String,
  value: String,
  name: String,
  isBodyWeight: Boolean
})

exports.GlobalExercises = mongoose.model('globalexercises', globalExercisesSchema)