const mongoose = require('../mongoose').mongoose

const workoutSchema = new mongoose.Schema({
  type: String,
  exercise: Array,
  date: Date,
  user_id: mongoose.ObjectId,
  creation_date: Date
})

exports.Workout = mongoose.model('workouts', workoutSchema)