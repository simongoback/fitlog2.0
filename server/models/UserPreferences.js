const mongoose = require('../mongoose').mongoose

const userPref = new mongoose.Schema({
  distanceUnit: String,
  timeUnit: String,
  weightUnit: String,
  exercises: Array,
  user_id: mongoose.ObjectId
})

exports.UserPreferences = mongoose.model('user_preferences', userPref)